% !TEX root = Douasbin_2017_AIAA.tex

    \subsection{Oscillator--Based Modelling\label{subsec:Oscillator Based Modelling}}
In the TDIBC framework, acoustic impedance can be prescribed in the Time--Domain.
The Time--Domain response of the impedance boundary condition fully relies on the impedance modelling in the Frequency--Domain.
This modelling is built as the superimposition of Mass--Spring Damped Harmonic Oscillators referred in this work as "Oscillators". 
The quality of impedance modelling is therefore critical and requires investigation for an optimal accuracy, especially in challenging application such as acoustic delays modelling.
An Iterative Multi--Oscillator Modelling Technique based on the Least--Square Method will be presented.
In this Section we will first explain in details the Multi--Oscillator Model.
The properties of a Single--Oscillator will be highlighted, leading to straightforward criteria to precisely control them. 
Specific constraints will be derived in the, otherwise difficult, case of delay modelling.
Finally, an Iterative Multi–Oscillator Modelling Technique will be presented.

\textcolor{blue}{Present plan for this section. Explain that/why we need to derive precise constraints. Talk about iterative fit procedure.  + Explain the plan of this section}

\subsubsection{Multi--oscillator model}


In order to recover the correct Time--Domain behavior of an acoustic boundary, impedance needs to be modelled.
The unbounded nature of the impedance $Z$ makes it an inconvenient mean of acoustic modelling whereas reflection coefficient compiles the same information in a bounded manner.
Another benefit of a reflection coefficient based approach is the straightforward link to characteristic waves imposed in the NSCBC framework.
Alternatively, TDIBC was initially developed using wall softness $S = R + 1$ as a modelling mean of the acoustic impedance \textcolor{blue}{(cite)}.
The reflection coefficient $R$ is defined in the interval $ R \in [-1, 1]$ and thus wall softness  $S \in [0, 2]$.
In this work, a fitting procedure has been derived indifferently of wall softness/reflection coefficient choice. 
In the following, the fitting function $F$ can either represent the wall softness $S$ or the reflection coefficient $R$.
In the explicit formulation of a TDIBC proposed by Fung and Ju, the fitting function F is analogous to a damped mass--spring oscillator like system refered it as "acoustic oscillator".
This approach has been validated by \textcolor{blue}{(fung and ju, scaloBodart, and dig into citations of Scalo + Fung)}.
For a more general use Lin et al. extended the modelling to $n_0$ oscillators, leading to the following fitting function:

    \begin{equation}
        F(\omega) = \sum^{n_0}_{k=1} \left[ \frac{\mu_k}{i \omega - p_k} + \frac{\mu_k^{*}}{i \omega - p_k^{*}} \right]
        \label{eq:fitting_function1}
    \end{equation}

Where $p_k$ and $\mu_k$ are the pole and residue of the $k$th oscillator, $n_0$ the number of oscillators, $i$ is the imaginary unit, $\omega$ the angular frequency and the superscript $^*$ denotes the complex conjugate.
Equation~\ref{eq:fitting_function1} can be recast by expressing the poles and residues in their algebraic form as $\mu_k = a_k + i b_k$ and $p_k = c_k + i d_k$ and $a_k, b_k, c_k, d_k \in  R$.
Additionally, in order to impose a physical near--DC acoustic behavior of the wall softness model, Lin et al. have shown that $b_k = - a_k c_k / d_k$, leading to:

    \begin{equation}
        F(\omega) =  \sum^{n_0}_{k=1} F_k(\omega) = \sum^{n_0}_{k=1} \frac{2 a_k i \omega}{- \omega^2 - 2 c_k i \omega + \left( c^2_k + d^2_k \right)}
        \label{eq:fitting_function2}
    \end{equation}
\subsubsection{Single--oscillator properties}
\begin{figure}
 	\begin{subfigmatrix}{2}% number of columns
	       \subfigure[]{\includegraphics{single_oscillator_Mod.pdf}}
	       \subfigure[]{\includegraphics{single_oscillator_Phase.pdf}}
	\end{subfigmatrix}
 	\caption{Modulus (a) and phase (b) of a single oscillator $F_k$ of resonant frequency $f_{0,k} = 100 \textnormal{ Hz}$ for $\mu_k = 350 + 235i$ and $p_k = -350 + 522i$.}
   	\label{fig:ExampleSingleOscill}
\end{figure}

Causality is insured for $\Re \left( p \right) = c_k < 0$.
In order to obtain a robust modelling procedure, accurate initial guesses have to be found.
This has been done by deriving algebraic constraints for each degree of freedom in Eq.~\ref{eq:fitting_function2}.
Figure~\ref{fig:ExampleSingleOscill} shows the modulus and phase of a single oscillator corresponding to one term of the sum in Eq.~\ref{eq:fitting_function2}.
\textcolor{blue}{Discuss the oscillator frequency behavior to impulse response, resonance, damping, ...}
\textcolor{blue}{Discuss the fact that it is not null at resonance and then it tends to zero which makes it practical for fitting applications, especially for the softness.}
\textcolor{blue}{Softness = good DC \& high frequency values / reflection coefficient based $\rightarrow$ bad DC behavior + ill--posed because at DC: $\omega^2 < c_k^2$ }
\textcolor{blue}{Possible to fit the reflection coefficient however it is easier to fit $S = R + 1$. This has no heavy down size for practical applications.}
From the damped mass--spring oscillator analogy, it has been shown \textcolor{blue}{(cite carlo)} that the resonant angular frequency $\omega_{0,k}$ can be expressed as: 

    \begin{equation}
        \omega_{0, k} = \sqrt{c_k^2 + d_k^2}
        \label{eq:algebraic_constraint2}
    \end{equation}
    
Leading to:

    \begin{equation}
        d_k = \sqrt{\omega_{0,k}^2 - c_k^2}
        \label{eq:algebraic_constraint3}
    \end{equation}
    
As shown in Fig.~\ref{fig:ExampleSingleOscill}, at the resonant angular frequency $\omega_{0,k}$ a single--oscillator $F_k$ is real valued.
Expressing $F_k$ at the resonant angular frequency $\omega_{0,k}$ and using Eq.~\ref{eq:algebraic_constraint3} gives:

    \begin{equation}
        F_k (\omega_{0, k})= h_{0,k} = - \frac{a_k}{c_k}
        \label{eq:constraint_peak_height}
    \end{equation} 

Where the peak value at resonance is denoted $h_{0,k}$.
Introducing Eqs.~\ref{eq:algebraic_constraint2} and \ref{eq:constraint_peak_height} in Eq.~\ref{eq:fitting_function2}, we obtain:

    \begin{equation}
        F(\omega) = \sum^{n_0}_{k=1} \frac{  2 h_{0,k}  c_k i \omega}{ \omega^2 + 2 c_k i \omega - \omega_{0,k}^2}
        \label{eq:fitting_function3}
    \end{equation}

\begin{figure}
 \begin{subfigmatrix}{2}% number of columns
  \subfigure[]{\includegraphics{Modulus_vs_width.pdf}}
  \subfigure[]{\includegraphics{phase_vs_width.pdf}}
 \end{subfigmatrix}
 \caption{Modulus (a) and phase (b) parts of single oscillator models for 3 different width. 
    Resonant frequency and peak height constraints (Eqs.~\ref{eq:algebraic_constraint2} and~\ref{eq:constraint_peak_height}) are fulfilled for $f_{0, k} = \omega_{0,k}/2 \pi  = 100$ Hz and $h_k(\omega_{0,k}) = - a_k / c_k =  1 + 0i$.
    The remaining degree of freedom $c_k$ is varied and its influence on the fitting function width is visualized.}
 \label{fig:oscWidth}
\end{figure}

%\subsubsection{Single--oscillator width constraint}\label{subsec:sgl_osc_width}

The $c_k$ parameter is the only remaining degree of freedom in Eq.~\ref{eq:fitting_function3}.
Figure~\ref{fig:oscWidth} highlights the effect of the coefficient $c_k$ on the width of a single--oscillator $F_k$ for a peak height $h_{0,k}=1$ at the resonant frequency $f_{0,k} = \omega_{0,k} / 2 \pi = 100$ Hz. 
The aim of this section is to present the link between $c_k$ and the peak width.
%Figure~\ref{subfig:fck_width} presents the real part of a single--oscillator as defined in Eq.~\ref{eq:fitting_function3}.
On can observe in Fig.~\ref{fig:oscWidth} that the maximum of a single--oscillator contribution is located at its resonant angular frequency $\omega_{0,k}$.
On both sides of this peak, $F_k$ decreases at a given rate and tends to zero far from the resonance angular frequency.
This rate of decay is controlled by the $c_k$ parameter (as shown in Fig.~\ref{fig:oscWidth}) and needs to be controlled for an accurate initial guess.
Figure~\ref{fig:ReImWidthDelay} illustrates the chosen definition for the width of a peak.
For a given $\epsilon \in [0, h_{0,k}]$ there are two solutions for the equation $\Re(F_k) = \epsilon$.
These solutions are denoted $\omega_-$ and $\omega_+$, with $\omega_- < \omega_{0,k} < \omega_+$.
The function $\Re(F_k)$ is asymmetric and decreasing less where $\omega > \omega_{0,k}$.
A major advantage of the single--oscillator model as a fitting function is that its contribution mainly occurs locally around resonance. 
In order to improve the robustness of the fitting procedure, one can take advantage of that property by choosing initial conditions that are of low global effect.
This can be acheived by using thin oscillators.
Therefore, the width of the peak will be approximated using the solution $\omega_+$, leading to an overestimation. 
Figure~\ref{fig:ReImWidthDelay} shows the evaluated width $\Delta {\omega_{c_k}}$ in grey calculated as $\Delta \omega_{c_k} = 2 \times (\omega_{+} - \omega_{0,k}) = 2 \Delta \omega_{\epsilon}$.
Using Eq.~\ref{eq:fitting_function3} and the definition of $\Delta \omega_{\epsilon}$:

    \begin{equation}
        \Re \left( F_k(\omega_{0,k} + \Delta \omega_{\epsilon}) \right) = \epsilon
        \label{eq:eq_to_solve_for_ck}
    \end{equation}
Soving Eq.~\ref{eq:eq_to_solve_for_ck} for a given width $\Delta \omega_{\epsilon}$ we obtain a constraint on $c_k$:

    \begin{equation}
        c_k(\epsilon, \Delta \omega_{\epsilon}) =  \frac{\omega_{0,k} \Delta \omega_{\epsilon} + \frac{1}{2} \Delta \omega^2_{\epsilon}}{\omega_{0,k} + \Delta \omega_{\epsilon}} \sqrt{\frac{\epsilon}{1 - \epsilon}} 
        \label{eq:eq_for_ck}
    \end{equation}
In practice, only low values of $\epsilon$ should be considered to insure a good control of the oscillator width.
Typical values of $\epsilon$ are of the order of magnitude $\epsilon \simeq 0.01h_{0,k}$

%Equation~\ref{eq:algebraic_constraint3} is a hard constraint at near--DC frequencies : low resonant frequency will result in imposing low $c_k$ values, leading therefore to a small width.
%However in practice this trouble can be overcome by a slight increase of the number of oscillators $n_0$.
%For a given oscillator the only remaining free parameter in therefore $c_k$.


\begin{figure}
    \begin{subfigmatrix}{2}% number of columns	
	\subfigure[Width $\Delta \omega_{\epsilon}$ for $\epsilon = 0.2$.]{\includegraphics{Real_part_Fk_ck_width.pdf}}
	    \label{fig:ReImWidthDelay_a}
       	\subfigure[Width $\Delta f_{\tau}$ for $\tau = 2$ ms.]{\includegraphics{Real_part_delay_width.pdf}}
	    \label{fig:ReImWidthDelay_b}
    \end{subfigmatrix}
    \caption{
    (a) Single--oscillator width definition $\Delta \omega_{c_k}$ (grey area) defined as $2 \Delta \omega_{\epsilon}$. 
    The value $\epsilon$ is an arbitrary percentage of the peak height $h_{0,k}$(here $\epsilon = 0.2$).
%    As the peak is asymmetric we define the total width $\Delta \omega_{c_k} \simeq 2 \Delta \omega_{\epsilon}$.
    (b) Definition of the width $\Delta \omega_{\tau}$, corresponding to the frequency range between 2 zeros of an acoustic delay $\tau$. 
    Here, the acoustic time delay $\tau = 2$ ms.
    }
    \label{fig:ReImWidthDelay}
\end{figure}

\subsubsection{Delayed Impedance constraint}\label{subsec:Delayed Impedance Constraint}

As seen in Section~\ref{subsec:impedance_modelling}, a known reflection coefficient $R_2$ of a boundary located at $z = z_2$ can be modelled in another location $z=z_1$.
The modelled reflection coefficient $R_{\tau} = R_2 \vert_{z = z_1}$ is linked to $R_2$ in the frequency--domain by the use of a complex exponential $e^{- i \omega \tau}$, where $\tau$ is the acoustic time delay of a wave to travel back and forth through the modelled segment $z_2 - z_1$, the $R_{\tau}$ is referred to as "delayed reflection coefficient".
The modelling of delayed reflection coefficients requires specific care as periodic functions are introduced in the frequency--domain by the use of complex exponential.
Figure~\ref{fig:ReImWidthDelay} shows the frequency range $\Delta f_{\tau}$ between two consecutive zeros that can be computed as:

    \begin{equation}
        \Delta \omega_{\tau} = 2 \pi \Delta f_{\tau} = \frac{(n+1)\pi}{\tau} - \frac{n \pi}{\tau} = \frac{\pi}{\tau}  
        \label{eq:eq_om_tau}
    \end{equation}
The periodicity occurs over a frequency range $\Delta f_{\tau}$ which is inversely proportional to the delay $\tau$.
As a consequence, the longer the time delay $\tau$ the more oscillators $n_0$ are required for the modelling. 
The periodicity introduced by the time delay in the Frequency--Domain is extremely challenging in terms of modelling.
If initial guesses are chosen poorly, \textit{e.g.} a too wide oscillator, the least--square fit algorithm will not converge to the global minimum error but rather to a local minimum. 
For a robust modelling procedure, it has been found necessary to impose a width sufficiently small to be contained in a half period $\Delta \omega_{\tau}$: 

	\begin{equation}
		2\Delta \omega_{\epsilon} < \Delta \omega_{\tau}
		\label{eq:width_criterion}
	\end{equation}
Introducing Eq.~\ref{eq:width_criterion} in Eq.~\ref{eq:eq_for_ck}, a criterion can be obtained for a delay $\tau$:

%    \begin{equation}
%        c_k(\epsilon)   < \frac{\frac{\pi}{\tau} \omega_{0,k} +  (\frac{1}{2}\frac{\pi}{\tau})^2   }{2 \omega_{0,k} + \frac{\pi}{\tau}} \sqrt{\frac{\epsilon}{1 - \epsilon}} 
%    \end{equation}
        
    \begin{equation}
        c_k(\epsilon, \omega_{0,k})   < \frac{
        \tau \omega_{0,k} + \frac{\pi}{4}  
        }
        {
        2 \pi \omega_{0,k} + \tau
        } 
        \sqrt{\frac{\epsilon}{1 - \epsilon} }
        \label{eq:width_delayed}
    \end{equation}

    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{algo_diag_crop_numbered.pdf}
        \caption{Algorithm of the iterative fitting.}
        \label{fig:algo_diagram}
    \end{figure}

    \subsection{Iterative Multi--Oscillator Modelling Technique}\label{subsec:Iterative Multi--Oscillator Modelling Technique}
    
%    High--quality impedance modelling can be acheived by a least--square fit.
    The aim of the modelling is to determine the residues and poles ($\mu_k$, $p_k$) in Eq.~\ref{eq:fitting_function1}.
A least--square fit algorithm is used to minimize the residual of both real and imaginary parts.
%    Impedance modelling as required can be a tedious task, especially in highly constrainted problems such as delayed impedance.
    If no specific care is taken, the fitting procedure may diverge.
    To improve its robustness three measures where found to be necessary: (1) increase iteratively the number of oscillators, (2) re-initialize the oscillators using fitted values, (3) add an oscillator using the precise algebraic relations described in Sec.~\ref{subsec:Oscillator Based Modelling}.
    Figure~\ref{fig:algo_diagram} presents the algorithm used in the fitting procedure.
    At each iteration the modelling consists in 6 steps as described in Fig~\ref{fig:algo_diagram}:
        \begin{itemize}
            \item Step 1 consists in increment the number of oscillators $n_0$ considered in Eq.~\ref{eq:fitting_function2}.
            \item Step 2 consists in expressing the real error function $E_R$: 
                \begin{equation}
                    E_R = \Re(R) - \Re(F)
                    \label{eq:error_fnct}
                \end{equation}
            \item Step 3 consists in identifying the maximum of $E_R$ and its corresponding angular frequency: 
                \begin{equation}
                    \max(E_R) = \Re(R(\omega_{E})) - \Re(F(\omega_{E}))
                    \label{eq:hk_omk_form_error_fnct}
                \end{equation}
            \item Step 4 consists in initalizing the new oscillator by using Eqs.~\ref{eq:algebraic_constraint3}, \ref{eq:constraint_peak_height} and Eq.~\ref{eq:eq_for_ck} in the general case and Eq.~\ref{eq:width_delayed} for the delayed case. The chosen angular frequency $\omega_{0,k}$ and resonant peak height $h_{0,k}$ are defined using the real error function $E_R$:                           
                \begin{equation}
                    h_{0,k} = \max(E_R)
                    \label{eq:hk_max_e_r}
                \end{equation}     
                \begin{equation}
                    \omega_{0,k} = \omega_E
                    \label{eq:om0_omE}
                \end{equation}              
            \item Step 5 consists in minimizing the least--square residual $S$ defined on both real and imaginary parts of $R$ and $F$.
            \item Step 6 is a conditional test: while the residual is higher that the target value the iterations process carries on.

\textcolor{blue}{Simple example of each steps to explain the steps as Laurent suggested?}

        \end{itemize}
    
