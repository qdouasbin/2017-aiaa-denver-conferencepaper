% !TEX root = Douasbin_2017_AIAA.tex
\subsection{Context}\label{subsec:Context}

\lettrine[nindent=0pt]{A}{coustically} well--defined boundary conditions are one of the major concerns in fully compressible Navier--Stokes simulations \cite{Poinsot:2011}. 
In this framework both Dirichlet and Neumann boundary condition are acoustically reflective \cite{Thompson:1990, Poinsot:1992b}. 
In presence of an acoustical source in the flow, \textit{e.g.} turbulence or combustion, these acoustic reflections can lead to numerical complications and unphysical results.
Both Direct Numerical Simulation (DNS) and Large Eddy Simulation (LES), the two main approaches used in high--fidelity compressible solvers, strive to lower numerical dissipation.
In this framework, the acoustic wave are propagated in the computational domain without energy loss.
The imposition of steady target values at the boundary conditions is creating fully--reflective acoustic conditions at the boundaries leading to an unphysical acoustic energy increase, which in turn endanger the numerical stability.
%Combining compressible DNS/LES with   boundary conditions then leaves the waves free to propagate but numerically forced to remain in the domain, leading to an unphysical acoustic energy increase, which in turn endanger the numerical stability.
On the other hand, perfectly non--reflecting conditions must also be avoided as they lead to ill-posed problems allowing for the mean flow to drift \cite{Poinsot, selle, Rudy and Strikwerda}. 
In this context low--reflection boundary conditions are needed.
This has been successfully achieved \cite{Poinsot:1992b} by the use of Navier--Stokes Characteristic Boundary Condition (NSCBC).
A tradeoff between perfectly non--reflective and mean flow imposition has to be found \cite{Selle:2004b}.
Another way of obtaining low reflection is to impose an extremely high viscosity at the boundary condition in order to damp all reflective waves \cite{Bake:2008, Bogey:2011, Pirozzoli:2013} \cite{Bogey:2011}.
However, acoustic reflection is a major modelling mean in acoustics: high--fidelity DNS/LES must be able to account for any physical reflection.
Physical boundaries such as acoustic liners have a peculiar acoustic response and therefore have to be modelled.
In other cases, connected acoustic elements have to be modelled in order to account for the acoustic behavior of the whole system. 
For instance, as aeronautical combustors are highly sensitive to acoustic waves \cite{Roux:2008}, the acoustic behavior of the connected elements (compressor, turbine, nozzle, multi-perforated panel, \textit{etc.}) have to be taken into account. 
Given the high computational cost of DNS/LES approaches, simulating the whole domain is, most of the time, out of reach: it is preferable to model these acoustic elements. 
Acoustic waves are the manifestation of perturbations around a mean state, \textit{e.g.} the pressure field can be decomposed as $p = p_0 + p^{'}$, where $p_0$ is the mean pressure and $p^{'}$ is the Time--Domain acoustic perturbation.
The characterization of an acoustic element is performed in the Frequency--Domain by the use of a complex transform, such as the Fourier transform $\mathcal{F}$, \textit{e.g.} the complex acoustic pressure is defined as $ p ( \omega ) = \mathcal{F} ( p^{'})$, where $\omega$ represents the angular frequency.
A widely-accepted quantity for characterizing an acoustic element is the impedance.
Impedance is defined in the Frequency--Domain as the non--dimensional ratio of complex pressure and velocity\cite{Munjal:1990}: 

\begin{equation}
	Z(\omega) = \frac{1}{\rho_0 c_0} \frac{p(\omega)}{u(\omega)}
	\label{eq:imp_def}
\end{equation}
Impedance is a frequency--dependant complex--valued function.
%As a consequence, it can not be directly imposed in temporal simulations.
%To tackle this issue TDIBC was developed.
%A schematic of a generic setup is made in Fig.~\ref{fig:1d_tube}.
%\begin{figure}[ht]
%\centering{\includegraphics[width=0.65\textwidth]{tube_acoustics.pdf}
%\caption{Schematic of the 1D acoustic in a tube. $A^-(\omega)$ and $A(\omega)^+$ are the complex wave amplitudes corresponding to the left and right travelling waves in the time--domain. The red dotted line is the physical boundary being described by the reflection coefficient $R(\omega)=A^-(\omega) / A^+(\omega)$.}
%\label{fig:1d_tube}}
%\end{figure}
Alternatively, the reflection coefficient can be used.
It is defined using the impedance and links the characteristic waves amplitudes:

\begin{equation}
	R(\omega) = \frac{1 - Z(\omega)}{1 + Z(\omega)} = \frac{{A}^{-}(\omega) }{ A^{+}(\omega) }  
	\label{eq:refl_coeff_def}
	\nomenclature{$A^{+}$}{Right travelling characteristic wave}
	\nomenclature{$A^{-}$}{Left travelling characteristic wave}
	\label{eq:reflection_coefficient_definition}
\end{equation}
\textcolor{blue}{Add definition of waves + Talking about outlet only (otherwise Eq. untrue) + Add simple Figure.}
The NSCBC framework being based on characteristics, the reflection coefficient will be preferred here.
Both impedance and reflection coefficient are defined in the Frequency--Domain whereas DNS/LES are performed in the Time--Domain.
Equation~\ref{eq:reflection_coefficient_definition} can be expressed in the Time--Domain by inverse Fourier transform, leading to the following convolution integral:  

\begin{equation}
    A^{-}(t) = \int_{-\infty}^{t} R(\tau)A^+(t - \tau) d\tau     
	\label{eq:convolution_integral}
\end{equation}
Equation~\ref{eq:convolution_integral} , although mathematically equivalent to Eq.~\ref{eq:reflection_coefficient_definition}, is not directly applicable in high--fidelity compressible solvers: 
	\begin{itemize}
		\item the direct evaluation of the convolution integral requires a large amount of memory.
		The values of $A^{+}(t)$ and $R(t)$ need to be stored at every iterations. 
		As a consequence, the amount of memory required increases in time,
		\item The associated CPU cost increases in time.
	\end{itemize}
In computationally intensive frameworks such as DNS/LES, a low--cost compact--in--time method  is needed for the evaluation of Eq.~\ref{eq:convolution_integral}.
This has been achieved by the TDIBC method developed by Fung and Ju\cite{Fung:2001,Fung:2004} and validated by several authors \cite{Scalo:2015, +LinJFM}.
% TDIBC methods allows a recursive evaluation of Eq.~\ref{eq:convolution_integral} in the Time--Domain.

\subsection{Time--Domain Impedance Boundary Condition}\label{subsec:Time--Domain Impedance Boundary Condition}
TDIBC allows the imposition of Time--Domain acoustic waves fulfilling acoustic properties defined in Frequency--Domain.
To accomplish this, the only pre-requisite is the modelling of a reflection coefficient based function in Frequency--Domain under partial fractions:
%This is performed by modelling the reflection coefficient (or wall softness) in the Laplace domain as a partial fraction representing an acoustic oscillator.
%This principle has been extended by Lin et al. \cite{Lin:2016} to a multi-oscillator technique:
\begin{eqnarray}
	R(\omega) = \sum_{k = 1}^{2 n_0} \frac{\mu_k}{i \omega - p_k}
	\label{eq:sum_oscill}
\end{eqnarray}
\textcolor{blue}{(I believe that it is simpler to explain under this form. We will need to talk about the conjugate pairs == oscillator in the Method part.)}
The Time--Domain equivalent of Eq.~\ref{eq:sum_oscill} is:
\begin{eqnarray}
	R(t) = \sum_{k = 1}^{2 n_0} \mu_k e^{p_kt}
	\label{eq:sum_oscill_TD}
\end{eqnarray}
Using Eq.~\ref{eq:sum_oscill_TD}, the convolution integral of Eq.~\ref{eq:convolution_integral} can be reformulated as a sum of sub--convolution integrals $I_k$: 
\begin{equation}
    A^{-}(t) = \sum_{k = 1}^{2 n_0} \int_{-\infty}^{t} \mu_k e^{p_k \tau} A^+(t - \tau) d\tau  = \sum_{k = 1}^{2 n_0} I_k(t)
	\label{eq:convolution_integral_sum_ik}
\end{equation}
The properties of Eq.~\ref{eq:sum_oscill_TD} allow a recursive evaluation of each sub--convolution integral: the value of $I_k(t)$ can be evaluated using its value at the previous computational time step $I_k(t - \Delta t)$:

\begin{equation} 
I_k(t) = z_k I_k(t - \Delta t )
\label{eq:final_formula_numerical2}
    +  
%\left[
\alpha_{k} A^+(t) + \beta_{k} A^+(t - \Delta t)
%\right]
\end{equation}

\begin{eqnarray}
    z_k =  e^{p_k \Delta t} 																	\quad ; \quad
    \alpha_{k} =  \mu_k \left(	\frac{z_k -1}{p_k^2 {\Delta t}} - \frac{1}{p_k} 	\right) \quad ; \quad
    \beta_{k}  =   \mu_k \left(	\frac{z_k -1}{p_k^2 {\Delta t}} - \frac{z_k}{p_k}	\right) 
\end{eqnarray}

Equations~\ref{eq:convolution_integral_sum_ik}~and~\ref{eq:final_formula_numerical2} provides a low--cost, memory efficient, compact--in--time method for the evaluation of the wave $A^{-}(t)$ in the Time--Domain fulfilling the Frequency--Domain acoustic properties of the boundary. 

%\nomenclature{$\rho_0$}{Mean flow density}%
%\nomenclature{$c_0$}{Mean flow sound speed}%
%
%\nomenclature{$i$}{Imaginary unit}
%\nomenclature{$s$}{Laplace variable}
%\nomenclature{$\omega$}{Fourier variable}
%
%\nomenclature{$p$}{Acoustic pressure}%
%\nomenclature{$u$}{Acoustic velocity}%
%\nomenclature{$Z$}{Impedance}%
%\nomenclature{$R$}{Reflection coefficient}

%\nomenclature{$\mu$}{Residue}
%\nomenclature{$p$}{Pole}

%\nomenclature[t]{$\widehat{}$}{Denotes a complex number}
%\nomenclature[t]{$^{*}$}{Denotes a complex conjugate}
%
%\nomenclature[b]{$_s$}{Expressed in Laplace Domain}
%\nomenclature[b]{$_{\omega}$}{Expressed in Fourier Domain}

\subsection{Delayed Impedance}\label{subsec:Delay Impedance}
%
%\textcolor{red}{
%\\
%------------------------------
%\\
%NOT DONE YET
%\\
%------------------------------
%\\
%}
%
%\textcolor{blue}{
%In numerous systems, acoustics plays a major role.
%For a large range of combustion systems, from lab experiment to gas turbines or rocket engines, the combustion dynamics is extensively sensitive to acoustic waves.
%The propagation of these waves propagation is greatly driven by the impedance $Z$ of the acoustically connected elements such as compressors, turbines, perforated plates, \textit{etc}.
%The impedance is defined in the frequency domain as $Z = p / \rho_0 c_0 u$, where $p$ and $u$ are the complex acoustic pressure and velocity, respectively, $\rho_0$ the flow density and $c_0$ the sound speed. 
%The acoustic waves propagated between two elements can be modelled using impedance. 
%Alternatively, the reflection coefficient $R = (Z + 1) / (Z - 1)$ can be used.
%A high--fidelity TDIBC should be able to characterize any kind of impedance which can be classified into three categories.
%First, simple limit cases such as $R = -1$, $R = 0$, $R = -1$ corresponding to zero acoustic velocity, non--reflecting and zero acoustic pressure conditions, respectively.
%Also, experimentally or numerically measured impedance need to be easily modelled.
%Finally, any of the above can be associated to a time delay corresponding to the propagation of acoustic waves in a longer geometry as shown in Fig.~\ref{fig:1d_tube}. 
%}
One of the advantages of impedance and reflection coefficient modelling is the translation theorem.
For instance, Fig.~\ref{fig:1d_tube} show a schematic of 1D linear duct acoustics.
\begin{figure}
    \centering{\includegraphics[width=\textwidth]{tube_acoustics_2parts_02.pdf}
    \caption{   
                Schematic of the 1D acoustic in a tube. 
                $A_{n}^-$ and $A_{n}^+$ are the wave amplitudes in the frequency domain corresponding to the left and right travelling waves in the time--domain in the $n$th part of the tube.
                The reflection coefficient is defined by $R_{n}=A_{n}^- / A_{n}^+$.
            }
    \label{fig:1d_tube}}
\end{figure}
The characteristic waves $A_{n}^-$ and $A_{n}^+$ can be used to define $R_{n}$ as defined in Eq.~\ref{eq:reflection_coefficient_definition}.
The translation theorem allows to express a reflection coefficient in another location using the time delay (acoustic propagation time in the Time--Domain).
The reflection coefficients $R_2$ (see Fig.~\ref{fig:1d_tube}) in $z = z_2$ as $R_2 = A_{2}^{-} / A_{2}^{+}$ can be expressed at location $z_1$ by a so called delayed reflection coefficient $R_{\tau}$ as follows:
    \begin{equation}
        R_{\tau} = R_2 \vert_{z = z_1} = R_2 e^{- i \omega \frac{2 (z_2 - z_1)}{c_0}} = R_2 e^{- i \omega \tau}
        \label{eq:delayed_reflection_coefficient}
    \end{equation}
Where $\tau$ is the time delay corresponding to the time for an acoustic wave to propagate back and forth over the distance $z_2 - z_1$ at a speed of sound $c_0$.
\textcolor{red}{\\Change $z$ in $x$ so it's consistent with the Results part/figures.\\}
\textcolor{blue}{
The modelling of delayed reflection coefficients as defined in Eq.~\ref{eq:delayed_reflection_coefficient} under the form proposed by Fung et al and Lin and Scalo is extremely challenging.
This challenge arises from the frequency--domain properties of the complex exponential in Eq.~\ref{eq:delayed_reflection_coefficient}.
The main objective of this paper is to create and validate a systematic modelling process to mimic any reflection coefficient. 
}

\textcolor{red}{\\\\\\
(Explain the plan of the paper Here.)
\\
------------------------------
\\
NOT DONE YET
\\
------------------------------
\\
}

